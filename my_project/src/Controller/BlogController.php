<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/blog", name="blog_")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("/{page<\d+>}", name="list")
     */
    public function list(Request $request, int $page = 1)
    {
        dump($request->getBaseUrl());
        return $this->render('blog/list.html.twig', [
            "page" => $page
        ]);
    }

    /**
     * @Route("/{slug}", name="show")
     */
    public function show(string $slug): Response
    {
        // $slug will equal the dynamic part of the URL
        // e.g. at /blog/yay-routing, then $slug='yay-routing'

        // ...
        return $this->render('blog/show.html.twig', [
            'slug' => $slug
        ]);
    }
}
