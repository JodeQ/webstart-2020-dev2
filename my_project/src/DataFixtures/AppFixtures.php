<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");

        for ($i = 0; $i < 10; $i++) {
            $category = new Category();
            $category->setName($faker->word());
            
            $manager->persist($category);
        }

        $manager->flush();

        $categories = $manager->getRepository(Category::class)->findAll();

        for ($i = 0; $i < 50; $i++) {
            $random = mt_rand(0, count($categories) - 1);
            $category = $categories[$random];

            $product = new Product();
            $product
                ->setName($faker->word)
                ->setPrice(mt_rand(100, 2500) / 100)
                ->setDescription($faker->realText(200))
                ->setCategory($category);

            $manager->persist($product);
        }

        $manager->flush();
    }
}
